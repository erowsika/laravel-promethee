<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Employee::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'nip' => $faker->randomNumber(3, true),
        'position' => $faker->text(5)
    ];
});

$factory->state(App\Models\Employee::class, 'with_relations', []);

$factory->afterCreatingState(App\Models\Employee::class, 'with_relations', function ($employee, $faker) {
    $employee->ranks()->save(
        factory(App\Models\Rank::class)->make()
    );

    $employee->groups()->save(
        factory(App\Models\Group::class)->make()
    );
});