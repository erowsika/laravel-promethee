<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'username' => $faker->unique()->userName,
        'email_verified_at' => now(),
        'password' => 'secret',
        'remember_token' => Str::random(10),
    ];
});

$factory->state(App\User::class, 'role_special', []);

$factory->state(App\User::class, 'role_ordinary', []);

$factory->afterCreatingState(App\User::class, 'role_special', function ($user, $faker) {
    $user->roles()->save(
        factory(App\Models\Role::class)->create(['role' => 'admin'])
    );
});

$factory->afterCreatingState(App\User::class, 'role_ordinary', function ($user, $faker) {
    $user->roles()->save(
        factory(App\Models\Role::class)->create()
    );
});
