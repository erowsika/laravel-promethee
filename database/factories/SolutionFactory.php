<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Solution::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'point' => rand(1,5)
    ];
});
