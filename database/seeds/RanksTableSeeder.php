<?php

use Illuminate\Database\Seeder;

class RanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ranks = config('helpers.seed.rank');

        foreach ($ranks as $key => $rank) {
            $new_rank = App\Models\Rank::create([
                'name' => $rank
            ]);

            if ($key === 0) $new_rank->delete();
        }
    }
}
