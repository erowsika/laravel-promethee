<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $group_seeds = config('helpers.seed.group');
        $rank_seeds = config('helpers.seed.rank');
        $employee_seeds = config('helpers.seed.employee');

        foreach ($employee_seeds as $key => $seed) {
            $employee = App\Models\Employee::create([
               'name' => $seed['name'],
               'nip' => (int) $seed['nip'],
               'position' => $seed['position']
            ]);

            $employee->groups()->attach(array_search($seed['group'], $group_seeds) + 1);
            $employee->ranks()->attach(array_search($seed['rank'], $rank_seeds) + 1);

            if ($key === 0) {
                $employee->groups()->detach();
                $employee->ranks()->detach();
                $employee->delete();
            }
        }
    }
}
