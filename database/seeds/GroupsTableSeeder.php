<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = config('helpers.seed.group');

        foreach ($groups as $key => $group) {
            $new_group = App\Models\Group::create([
                'name' => $group
            ]);

            if ($key === 0) $new_group->delete();
        }
    }
}
