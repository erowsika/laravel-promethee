<?php

use Illuminate\Database\Seeder;

class RoleUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // pejabat pimpinan dengan role admin
        DB::table('role_user')->insert([
            'user_id' => 1,
            'role_id' => 1
        ]);
        
        // pejabat sekretaris dengan role admin
        DB::table('role_user')->insert([
            'user_id' => 2,
            'role_id' => 1
        ]);
        
        // pejabat karyawan dengan role user
        DB::table('role_user')->insert([
            'user_id' => 3,
            'role_id' => 2
        ]);
        
        // admin dengan role admin
        DB::table('role_user')->insert([
            'user_id' => 4,
            'role_id' => 1
        ]);
    }
}
