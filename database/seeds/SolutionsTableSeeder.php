<?php

use Illuminate\Database\Seeder;

class SolutionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Solution::insert([
            [
                'name' => 'Dump',
                'point' => '2'
            ],
            [
                'name' => 'Kesetiaan',
                'point' => '8'
            ],
            [
                'name' => 'Prestasi kerja',
                'point' => '7'
            ],
            [
                'name' => 'Tanggung jawab',
                'point' => '6'
            ],
            [
                'name' => 'Ketaatan',
                'point' => '5'
            ],
            [
                'name' => 'Kejujuran',
                'point' => '4'
            ],
            [
                'name' => 'Kerja sama',
                'point' => '3'
            ],
            [
                'name' => 'Prakarsa',
                'point' => '2'
            ],
            [
                'name' => 'Kepemimpinan',
                'point' => '1'
            ]
        ]);

        App\Models\Solution::findOrFail(1)->delete();
    }
}
