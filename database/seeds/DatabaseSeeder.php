<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(RoleUserTableSeeder::class);
        $this->call(SolutionsTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
        $this->call(RanksTableSeeder::class);
        $this->call(EmployeesTableSeeder::class);
    }
}
