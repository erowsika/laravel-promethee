<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThresholdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thresholds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('solution_id');
            $table->enum('extrema', ['min', 'max']);
            $table->enum('type', ['1', '2', '3', '4', '5', '6']);
            $table->double('p')->default(0);
            $table->double('q')->default(0);
            $table->double('s')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('thresholds');
    }
}
