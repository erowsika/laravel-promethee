<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false, 'forget' => false, 'verify' => false]);


Route::middleware('auth')->group(function ()
{
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    Route::middleware('admin')->group(function ()
    {
        Route::resources([
            'user' => 'UserController',
            'employee' => 'EmployeeController',
            'solution' => 'SolutionController',
            'evaluation' => 'EvaluationController',
            'evaluation_datetime' => 'EvaluationDatetimeController',
        ], [ 'except' => 'show' ]);

        Route::get('evaluation_primary_keys', 'EvaluationController@getPrimaryKeys');

    });

});
