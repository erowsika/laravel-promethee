<?php
return [
    [
        'name' => 'Really Dump Value',
        'nip' => '327623551',
        'rank' => 'Penata Tingkat I',
        'group' => 'III d',
        'position' => 'Really Dump Value'
    ],
    [
        'name' => 'Mahfuddin Gassing, S.E.',
        'nip' => '195408311985031006',
        'rank' => 'Penata Tingkat I',
        'group' => 'III d',
        'position' => 'Kasi Pembina Kelurahan/Desa'
    ],
    [
        'name' => 'M. Arsyad Umar, S.Pd.',
        'nip' => '195103071986111001',
        'rank' => 'Penata Tingkat I',
        'group' => 'III d',
        'position' => 'Kasi Pemerintahan'
    ],
    [
        'name' => 'Bahtiar Usman, S.Sos.',
        'nip' => '195312971985931011',
        'rank' => 'Penata Tingkat I',
        'group' => 'III d',
        'position' => 'Kasi Pemberdayaan Masyarakat'
    ],
    [
        'name' => 'Syakhrir, S.Sos.',
        'nip' => '195607161991021003',
        'rank' => 'Penata Tingkat I',
        'group' => 'III b',
        'position' => 'Kasi Ketentraman'
    ],
    [
        'name' => 'Asmi, S.Sos.',
        'nip' => '196505282010012011',
        'rank' => 'Penata Muda Tingkat I',
        'group' => 'III c',
        'position' => 'Kasubag Umum & Kepegawaian'
    ],
    [
        'name' => 'Andi Harni Riska W, S.T.',
        'nip' => '197404222006042011',
        'rank' => 'Penata Muda Tingkat I',
        'group' => 'III b',
        'position' => 'Kasubag Perencanaan & Keuangan'
    ],
    [
        'name' => 'Syamsul',
        'nip' => '198208261986111001',
        'rank' => 'Pengatur Muda Tingkat I',
        'group' => 'II b',
        'position' => 'Staff'
    ],
    [
        'name' => 'Muhammad Saleh',
        'nip' => '197710032014091001',
        'rank' => 'Pengatur Muda',
        'group' => 'II a',
        'position' => 'Staff'
    ],
    [
        'name' => 'Ruslan',
        'nip' => '197702172009011002',
        'rank' => 'Pengatur Muda Tingkat I',
        'group' => 'II b',
        'position' => 'Staff'
    ],
    [
        'name' => 'Muhammad Yacub',
        'nip' => '197012311988021013',
        'rank' => 'Pengatur Tingkat I',
        'group' => 'II d',
        'position' => 'Staff'
    ],
    [
        'name' => 'Ramli',
        'nip' => '198705292014071001',
        'rank' => 'Pengatur Muda',
        'group' => 'II a',
        'position' => 'Staff'
    ],
    [
        'name' => 'Nurdiana Wahab',
        'nip' => '197406052009012004',
        'rank' => 'Pengatur',
        'group' => 'II c',
        'position' => 'Staff'
    ],
    [
        'name' => 'Junaid',
        'nip' => '197305042014071001',
        'rank' => 'Pengatur Muda',
        'group' => 'II a',
        'position' => 'Staff'
    ],
    [
        'name' => 'Hadiah',
        'nip' => '197005242014102001',
        'rank' => 'Pengatur Muda',
        'group' => 'II a',
        'position' => 'Staff'
    ],
    [
        'name' => 'Murni',
        'nip' => '197802222014072001',
        'rank' => 'Pengatur Muda',
        'group' => 'II a',
        'position' => 'Staff'
    ]
];