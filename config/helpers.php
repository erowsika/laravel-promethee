<?php

return [
    'seed' => [
        'group' => include_once(__DIR__.'/../helpers/GroupSeeds.php'),
        'rank' => include_once(__DIR__.'/../helpers/RankSeeds.php'),
        'employee' => include_once(__DIR__.'/../helpers/EmployeeSeeds.php')
    ]
];
