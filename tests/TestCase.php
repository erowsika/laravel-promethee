<?php

namespace Tests;

use App\User;
use App\Testing\Concerns\WithoutFeatureMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication,
        DatabaseMigrations,
        DatabaseTransactions,
        WithoutFeatureMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->disableFeatureMiddleware();

        $this->be(new User);
    }
}
