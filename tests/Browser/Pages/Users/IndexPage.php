<?php

namespace Tests\Browser\Pages\Users;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class IndexPage extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/user';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@deleteButton' => 'table tbody tr td input[type=submit]',
        ];
    }
}
