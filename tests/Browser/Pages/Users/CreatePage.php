<?php

namespace Tests\Browser\Pages\Users;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class CreatePage extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/user/create';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url())
                ->assertSee('Register');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@username' => 'input[name=username]',
            '@password' => 'input[name=password]',
            '@password-confirmation' => 'input[name=password_confirmation]',
            '@role' => 'select[name=role]',
        ];
    }

    /**
     * Attempt to login with the given credentials.
     *
     * @param  \Laravel\Dusk\Browser  $browser
     * @param  array  $user
     * @return void
     */
    public function createUser(Browser $browser, $user)
    {
        $browser->type('@username', $user['username'])
            ->type('@password', $user['password'])
            ->type('@password-confirmation', $user['password_confirmation'])
            ->select('@role', $user['role'])
            ->press('Register');
    }
}
