<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

class Login extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/login';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@username' => 'input[name=username]',
            '@password' => 'input[name=password]'
        ];
    }

    /**
     * Attempt to login with the given credentials.
     *
     * @param  \Laravel\Dusk\Browser  $browser
     * @param  string  $username
     * @param  string  $password
     * @return void
     */
    public function attempt(Browser $browser, string $username, string $password)
    {
        $browser->type('@username', $username)
            ->type('@password', $password)
            ->press('Login');
    }
}
