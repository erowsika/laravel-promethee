<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Login;

class LoginTest extends DuskTestCase
{
    public function testLoginWithCorrectCredentials()
    {
        $user = $this->user();

        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit(new Login)
                ->attempt($user->username, 'secret')
                ->assertPathIs('/dashboard')
                ->assertSee('Dashboard');
        });
    }

    public function testLoginWithIncorrectCredentials()
    {
        $user = factory(User::class)->make([
            'username' => 'foo',
            'password' => 'secret'
        ]);

        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit(new Login)
            ->attempt($user->username, 'incorrect')
            ->assertPathIs('/login')
            ->assertSee('These credentials do not match our records');
        });
    }
}
