<?php

namespace Tests\Browser;

use App\User;
use App\Models\Role;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Support\Facades\Auth;
use Tests\Browser\Pages\Users\IndexPage as UsersPage;

class UsersPageTest extends DuskTestCase
{
    public function testShouldSeeThingsOnUsersPage()
    {
        $currentUser = $this->user();

        $this->browse(function (Browser $browser) use ($currentUser) {
            $browser->login()
                    ->visit(new UsersPage)
                    ->assertSee('Users - '.User::count())
                    ->assertSee($currentUser->username)
                    ->assertSee($currentUser->roles()->first()->role)
                    ->assertSeeLink('Create')
                    ->assertSeeLink('Edit');
        });
    }

    public function testShouldSeeSuccessOnClickDeleteButton()
    {
        $newUser = factory(User::class)->states('role_special')->create();

        $this->browse(function (Browser $browser) use ($newUser) {

            $browser->loginAs($newUser->id)
                    ->visit(new UsersPage)
                    ->click('@deleteButton')
                    ->assertSee('Successfully Delete User !');
        });
    }

    public function testOnClickCreateShouldGoToCreateUserRoute()
    {
        $this->browse(function (Browser $browser) {

            $browser->login()
                    ->visit(new UsersPage)
                    ->clickLink('Create')
                    ->assertSee('Create User');
        });
    }
}
