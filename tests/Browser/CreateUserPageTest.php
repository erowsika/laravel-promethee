<?php

namespace Tests\Browser;

use App\User;
use App\Models\Role;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Users\CreatePage as CreateUserPage;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CreateUserPageTest extends DuskTestCase
{
    /**
     * @dataProvider userProvider
     */
    public function testCreateUser($user, $expected)
    {
        $this->browse(function (Browser $browser) use ($user, $expected) {
            $browser->login()
                    ->visit(new CreateUserPage)
                    ->createUser($user)
                    ->assertSee($expected);
        });
    }

    public function userProvider()
    {
        $this->refreshApplication();

        /**
         * @var array $usernames
         */
        $users = factory(User::class, 5)->make();

        /**
         * Get current username from last made users.
         *
         * @var array $currentUsername
         */
        $currentUsername = value(function () use ($users) {
            return $users->pop()->toArray();
        });

        /**
         * Set base user attributes for base case.
         *
         * @var array $currentUsername
         */
        $user = array_merge(
            $currentUsername,
            [
                'password' => 'secret',
                'password_confirmation' => 'secret',
                'role' => 2,
            ]
        );

        /**
         * Callback used to extend base case.
         *
         * @var array $currentUsername
         */
        $currentUser = function ($test) use ($user, $currentUsername) {
            return array_merge($user, $currentUsername, $test);
        };

        return [
            [ $user, 'Succesfully Created New User !' ],
            [ with(['username' => '  '], $currentUser), 'The username field is required.' ],
            [ with(['password' => '  '], $currentUser), 'The password field is required.' ],
            [ with(['password' => 'foo'], $currentUser), 'The password must be at least 6 characters.' ],
            [ with(['password_confirmation' => 'ssecret'], $currentUser), 'The password confirmation does not match.'],
        ];
    }
}
