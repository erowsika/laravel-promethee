<?php

namespace Tests;

use App\User;
use App\Models\Role;
use Laravel\Dusk\TestCase as BaseTestCase;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Illuminate\Foundation\Testing\DatabaseMigrations;

abstract class DuskTestCase extends BaseTestCase
{
    use CreatesApplication,
        DatabaseMigrations;

    /**
     * @var App\User $currentUser
     */
    protected $currentUser;

    public function setUp()
    {
        parent::setUp();

        $this->currentUser = factory(User::class)->states('role_special')->create();
    }

    public function tearDown()
    {
        static::$browsers->each(function ($browser) {
            $browser->driver->manage()->deleteAllCookies();
        });

        parent::tearDown();
    }

    /**
     * 
     * Current authentication user 
     * 
     * @return App\User
     */
    protected function user()
    {
        // use $currentUser
        // instead of creating new user from factory
        // that will cause increase number of created user
        // every time use login() for browser instences
        return $this->currentUser;
    }

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare()
    {
        static::startChromeDriver();
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    protected function driver()
    {
        $options = (new ChromeOptions)->addArguments([
            '--disable-gpu',
            '--headless'
        ]);

        return RemoteWebDriver::create(
            'http://localhost:9515', DesiredCapabilities::chrome()->setCapability(
                ChromeOptions::CAPABILITY, $options
            )
        );
    }
}
