<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Solution;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SolutionControllerTest extends TestCase
{
    public function testIndex()
    {
        $response = $this->get('/solution');
        $response->assertOk();

        factory(Solution::class, 11)->create();
        $response = $this->get('/solution');
        $response->assertOk()
                ->assertSee('pagination');
    }

    public function testCreate()
    {
        $response = $this->get('/solution/create');

        $response->assertOk();
    }

    public function testStoreThrowExceptionOnInvalidRequest()
    {
        $this->expectException(ValidationException::class);

        $solution = factory(Solution::class)->make(['name' => 1]);
        $requestNotValid = $solution->getAttributes();

        $this->withoutExceptionHandling()
            ->post('/solution', $requestNotValid);
    }

    public function testStore()
    {
        $solution = factory(Solution::class)->make();

        $response = $this->post('/solution', $solution->getAttributes());

        $response->assertRedirect('/solution/create')
            ->assertSessionHas('success');

        tap(Solution::first(), function ($created_solution) use ($solution) {
            $this->assertEquals($solution->getAttributes(), array_only($created_solution->getAttributes(), ['name', 'point']));
        });
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage 
     */
    public function testShow()
    {
        $this->withoutExceptionHandling()
            ->get('/employee/1');
    }

    public function testEdit()
    {
        $solution = factory(Solution::class)->create();

        $response = $this->get("/solution/{$solution->id}/edit");

        $response->assertOk();
    }

    public function testUpdateThrowExceptionOnInvalidRequest()
    {
        $solution = factory(Solution::class)->create();
        $solutionWitNumericName = factory(Solution::class)->make(['id' => 1, 'name' => 1]);

        $this->expectException(ValidationException::class);
        $this->withoutExceptionHandling()
            ->put("/solution/{$solution->id}", $solutionWitNumericName->getAttributes());
    }

    public function testUpdate()
    {
        $solution = factory(Solution::class)->create(['name' => 'foo']);
        $solutionOldName = $solution->name;

        $request = ['name' => 'bar'];

        $response = $this->from("/solution/{$solution->id}/edit")
            ->put("/solution/{$solution->id}", $request);

        $response->assertRedirect("/solution/{$solution->id}/edit")
        ->assertSessionHas('success');
        
        tap($solution->fresh(), function ($solution) use ($solutionOldName) {
            $this->assertNotEquals($solutionOldName, $solution->name);
        });
    }

    public function testDestroy()
    {
        $solution = factory(Solution::class)->create();

        $response = $this->delete("/solution/{$solution->id}");

        $response->assertRedirect('/solution')
        ->assertSessionHas('success');

        $this->assertEquals(0, Solution::count());
    }

    public function testDestroyThrowExceptionOnInvalidSolution()
    {
        $this->expectException(ModelNotFoundException::class);
        $this->withoutExceptionHandling()
            ->delete("/solution/2");
    }
}
