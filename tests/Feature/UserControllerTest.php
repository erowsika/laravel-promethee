<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use App\Models\Role;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserControllerTest extends TestCase
{
    public function testIndex()
    {
        $response = $this->get('/user');
        $response->assertOk();

        factory(User::class, 11)->states('role_ordinary')->create();
        $response = $this->get('/user');
        $response->assertOk()
                ->assertSee('pagination');
    }

    public function testCreate()
    {
        $response = $this->get('/user/create');

        $response->assertOk();
    }

    public function testStoreThrowExceptionOnAttributesWithOutRole()
    {
        $this->expectException(ValidationException::class);

        $user = factory(User::class)->make();

        $attributesWithOutRole = $user->getAttributes();

        $this->withoutExceptionHandling()
            ->post('/user', $attributesWithOutRole);
    }

    public function testStore()
    {
        $user = factory(User::class)->make();
        $role = factory(Role::class)->create();

        $attributes = array_merge(
            $user->getAttributes(), 
            [
                'role' => $role->id,
                'password_confirmation' => $user->password
            ]
        );

        $response = $this->post('/user', $attributes);

        $response->assertRedirect('/user/create')
            ->assertSessionHas('success');

        tap(User::first(), function ($user) use ($role) {
            $this->assertTrue($user->roles->contains($role));
        });
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage 
     */
    public function testShow()
    {
        $this->withoutExceptionHandling()
            ->get('/employee/1');
    }

    public function testEdit()
    {
        $user = factory(User::class)->states('role_ordinary')->create();

        $response = $this->get("/user/{$user->id}/edit");

        $response->assertOk();
    }

    public function testUpdateThrowExceptionOnInvalidAttributes()
    {
        $this->expectException(ValidationException::class);

        $user = factory(User::class)->create();
        $role = $user->roles()->save(factory(Role::class)->make());

        $attributesWithOutRole = $user->getAttributes();

        $this->withoutExceptionHandling()
            ->put("/user/{$user->id}", $attributesWithOutRole);
    }

    public function testUpdate()
    {
        $user = factory(User::class)->create();

        $userOldUsername = $user->username;

        $role = $user->roles()->save(factory(Role::class)->make());

        $response = $this->put("/user/{$user->id}", ['username' => 'bar', 'role' => $role->id]);

        $response->assertRedirect("/user/{$user->id}/edit")
            ->assertSessionHas('success');

        tap($user->fresh(), function ($user) use ($role, $userOldUsername) {
            $this->assertNotEquals($userOldUsername, $user->username);
            $this->assertTrue($user->roles->contains($role));
        });
    }

    public function testDestroy()
    {
        $user = factory(User::class)->create();

        $role = $user->roles()->save(factory(Role::class)->make());

        $response = $this->delete("/user/{$user->id}");

        $response->assertRedirect('/user')
            ->assertSessionHas('success');

        $this->assertEquals(0, User::count());
        $this->assertEquals(0, \DB::table('role_user')->get()->count());
    }

    public function testDestroyThrowExceptionOnInvalidUser()
    {
        $this->expectException(ModelNotFoundException::class);
        $this->withoutExceptionHandling()
            ->delete("/user/2");
    }
}
