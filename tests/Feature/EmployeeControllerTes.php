<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Rank;
use App\Models\Group;
use App\Models\Employee;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EmployeeControllerTes extends TestCase
{
    public function testIndex()
    {
        $response = $this->get('/employee');
        $response->assertOk();

        factory(Employee::class, 11)->states('with_relations')->create();
        $response = $this->get('/employee');
        $response->assertOk()
                ->assertSee('pagination');
    }

    public function testCreate()
    {
        $response = $this->get('/employee/create');

        $response->assertOk();
    }

    public function testStoreThrowExceptionOnInvalidAttributes()
    {
        $this->expectException(ValidationException::class);

        $employee = factory(Employee::class)->make();
        $attributesNotValid = $employee->getAttributes();

        $this->withoutExceptionHandling()
            ->post('/employee', $attributesNotValid);
    }

    public function testStore()
    {
        $rankModel = factory(Rank::class)->create();
        $groupModel = factory(Group::class)->create();
        $employee = factory(Employee::class)->make(['nip' => 123456]);

        $rank = $rankModel->id;
        $group = $groupModel->id;

        $attributes = array_merge(
            compact('rank'),
            compact('group'),
            $employee->getAttributes()
        );

        $response = $this->post('/employee', $attributes);

        $response->assertRedirect('/employee/create')
            ->assertSessionHas('success');

        tap(Employee::first(), function ($employee) use ($rankModel, $groupModel) {
            $this->assertTrue($employee->ranks->contains($rankModel));
            $this->assertTrue($employee->groups->contains($groupModel));
        });
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage 
     */
    public function testShow()
    {
        $this->withoutExceptionHandling()
            ->get('/employee/1');
    }

    public function testEdit()
    {
        $employee = factory(Employee::class)->create(['nip' => 123456]);

        $employee->ranks()->save(factory(Rank::class)->make());

        $employee->groups()->save(factory(Group::class)->make());

        $response = $this->get("/employee/{$employee->id}/edit");

        $response->assertOk();
    }

    public function testUpdateThrowExceptionOnInvalidAttributes()
    {
        $employeeWitNullNip = factory(Employee::class)->create();
        $rank = $employeeWitNullNip->ranks()->save(factory(Rank::class)->make());
        $group = $employeeWitNullNip->groups()->save(factory(Group::class)->make());

        $this->expectException(ValidationException::class);
        $this->withoutExceptionHandling()
            ->put("/employee/{$employeeWitNullNip->id}", $employeeWitNullNip->getAttributes());
    }

    public function testUpdate()
    {
        $employee = factory(Employee::class)->create(['nip' => 123456]);
        $employeeOldName = $employee->name;
        $rank = $employee->ranks()->save(factory(Rank::class)->make());
        $group = $employee->groups()->save(factory(Group::class)->make());

        $attributes = ['name' => 'bar', 'rank' => $rank->id, 'group' => $group->id];

        $response = $this->from("/employee/{$employee->id}/edit")
            ->put("/employee/{$employee->id}", $attributes);

        $response->assertRedirect("/employee/{$employee->id}/edit")
        ->assertSessionHas('success');
        
        tap($employee->fresh(), function ($employee) use ($rank, $group, $employeeOldName) {
            $this->assertNotEquals($employeeOldName, $employee->name);
            $this->assertTrue($employee->ranks->contains($rank));
            $this->assertTrue($employee->groups->contains($group));
        });
    }

    public function testDestroy()
    {
        $employee = factory(Employee::class)->create();
        
        $rank = $employee->ranks()->save(factory(Rank::class)->make());
        $group = $employee->groups()->save(factory(Group::class)->make());
        
        $response = $this->delete("/employee/{$employee->id}");
        
        $response->assertRedirect('/employee')
        ->assertSessionHas('success');
        
        $this->assertEquals(0, Employee::count());
        $this->assertEquals(0, \DB::table('employee_rank')->get()->count());
        $this->assertEquals(0, \DB::table('employee_group')->get()->count());
    }

    public function testDestroyThrowExceptionOnInvalidEmployee()
    {
        $this->expectException(ModelNotFoundException::class);
        $this->withoutExceptionHandling()
            ->delete("/employee/2");
    }
}
