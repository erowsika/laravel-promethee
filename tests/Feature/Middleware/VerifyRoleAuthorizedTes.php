<?php

namespace Tests\Feature\Middleware;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Middleware\VerifyRoleAuthorized;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Exceptions\UnautherizedOrdinaryRoleException;

class VerifyRoleAuthorizedTes extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->withMiddleware(VerifyRoleAuthorized::class);
    }

    public function testThrowExceptionOnUnatherizedRoleOrdinary()
    {
        $this->expectException(UnautherizedOrdinaryRoleException::class);

        $this->withoutExceptionHandling()
            ->actingAs(factory(User::class)->states('role_ordinary')->create())
            ->get('/user/create');
    }

    public function testMiddlewareWillNotAllowUserOrdinaryRole()
    {
        $response = $this->actingAs(factory(User::class)->states('role_ordinary')->create())
                        ->get('/user/create');

        $response->assertForbidden();
    }

    public function testMiddlewareWillAllowUserSpecialRole()
    {
        $response = $this->actingAs(factory(user::class)->states('role_special')->create())
                    ->get('/user/create');

        $response->assertOk();
    }
}
