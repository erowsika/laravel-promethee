document.addEventListener('DOMContentLoaded', () => document.querySelector('select[name="group"]').onchange = changeEventHandler, false);

function changeEventHandler(event) {
    if (event.target.value) {
        document.querySelector('#rank').value = event.target.value.toString();
        document.querySelector('input[name=rank]').value = event.target.value.toString();
    }
}
