@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif

                    You are logged in!

                    <ul class="ststk">
                        <li>Total User <span class="badge">4</span></li>
                        <li><a href="#">Total Pegawai <span class="badge">15</span></a></li>
                        <li><a href="#">Total Kriteria <span class="badge">8</span></a></li>
                        <li><a href="#">Total Hasil Penilaian <span class="badge">0</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
