@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Kriteria List - <span>{{ $solutions->count() }}</span>  <span><a href="{{ route('solution.create') }}" class="button">Add</a></span></div>

                <div class="panel-body">
                    @if (session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Nama Kriteria</th>
                                <th>Bobot</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($solutions as $solution)
                            <tr>
                                <td>{{ $solution->name }}</td>
                                <td>{{ $solution->point  }}</td>
                                <td>
                                    <a href="{{ route('solution.edit', [ 'solution' => $solution->id ]) }}">Edit</a>
                                    <form action="{{ route('solution.destroy', [ 'solution' => $solution->id ]) }}" method="POST">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <input type="submit" value="Delete">
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $solutions->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
