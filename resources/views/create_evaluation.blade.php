@extends('layouts.app')

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="container-fluid">
    <div class="row">
        <form action="{{ route('evaluation.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Evaluation Datetime</div>

                <div class="panel-body">
                    @if (session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif

                    <input type="datetime" name="datetime" id="datetime">
                </div>
            </div>
        </div>
        
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Evaluation</div>
                    <table class="table" style="table-layout: fixed;">
                        <tr>
                            <td rowspan="{{ count($employees) + 2}}" class="vertical-text" align="center" valign="middle" style="vertical-align: middle;">TOPSIS &#x26; Promethee</td>
                            <td rowspan="2">No.</td>
                            <td rowspan="2">Employee</td>
                            <td colspan="{{ count($solutions) }}">Solution</td>
                        </tr>

                        <tr>
                            @foreach ($solutions as $solution)
                            <td>{{ $solution->name }}</td>
                            @endforeach
                        </tr>

                        @foreach ($employees as $employee)
                        <tr>
                            <td width="1%">{{ $loop->iteration }}</td>
                            <td width="25%">{{ $employee->name }}</td>
                            @foreach ($solutions as $solution)
                            <td>
                                <p class="field">
                                    <input class="input" name="solutions" type="number" placeholder="..." style="width: 100%;">
                                </p>
                            </td>
                            @endforeach
                        </tr>
                        @endforeach

                        <tr>
                            <td rowspan="5" class="vertical-text" align="center" valign="middle" style="vertical-align: middle;">Promethee</td>
                            <td colspan="2">Min/Max</td>
                            @foreach ($solutions as $solution)
                            <td>
                                <p class="field">
                                    <span class="select">
                                        <select name="extrema">
                                            <option value="min">Min</option>
                                            <option selected value="max">Max</option>
                                        </select>
                                    </span>
                                </p>
                            </td>
                            @endforeach
                        </tr>

                        <tr>
                            <td colspan="2">Type</td>
                            @foreach ($solutions as $solution)
                            <td>
                                <p class="field">
                                    <span class="select">
                                        <select name="types">
                                            <option value="1">I</option>
                                            <option value="2">II</option>
                                            <option value="3">III</option>
                                            <option value="4">IV</option>
                                            <option selected value="5">V</option>
                                            <option value="6">VI</option>
                                        </select>
                                    </span>
                                </p>
                            </td>
                            @endforeach
                        </tr>

                        <tr>
                            <td rowspan="3">Thresholds</td>
                            <td>p</td>
                            @foreach ($solutions as $solution)
                            <td>
                                <p class="field">
                                    <input class="input" type="number" placeholder="..." name="thresholds[p]" style="width: 100%;" value="98">
                                </p>
                            </td>
                            @endforeach
                        </tr>

                        <tr>
                            <td>q</td>
                            @foreach ($solutions as $solution)
                            <td>
                                <p class="field">
                                    <input class="input" type="number" placeholder="..." name="thresholds[q]" style="width: 100%;" value="10">
                                </p>
                            </td>        
                            @endforeach
                        </tr>

                        <tr>
                            <td>s</td>
                            @foreach ($solutions as $solution)
                            <td>
                                <p class="field">
                                    <input class="input" type="number" placeholder="..." name="thresholds[s]" style="width: 100%;" value="0">
                                </p>
                            </td>        
                            @endforeach
                        </tr>
                    </table>
                    <div class="panel-footer">
                        <input type="submit">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('scripts')
    <script type="text/javascript">
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) dd = '0' + dd; 
        if (mm < 10) mm = '0' + mm;
        var today = yyyy + '-' + mm + '-' + dd;
        document.querySelector('input[name="datetime"]').value = today;

        var random = function () { return Math.floor(Math.random() * 90 + 10); }
        var elements = document.querySelectorAll('input[name="solutions"]');
        var tp = document.querySelectorAll('input[name="thresholds[p]"]');
        var tq = document.querySelectorAll('input[name="thresholds[q]"]');
        var ts = document.querySelectorAll('input[name="thresholds[s]"]');
        var te = document.querySelectorAll('select[name="extrema"]');
        var tt = document.querySelectorAll('select[name="types"]');
        var i = j = flag = 0;
        var e = {{ count($employees) }} - 1;
        var s = {{ count($solutions) }} - 1;
        var setOfIndexedEmployee, setOfIndexedSolution;
        axios.get('{{ url("evaluation_primary_keys") }}')
        .then(function (res) {
            setOfIndexedEmployee = res.data[0];
            setOfIndexedSolution = res.data[1];
            
            for (var element of elements) {
                if (i > e) i = 0;
                if (j > s) {
                    flag = 1;
                    j = 0; i++; 
                }
                element.value = random();
                element.name = 'evaluations['+ i +']['+ j +'][point]';
                $('form').append('<input type="hidden" name="evaluations['+ i +']['+ j +'][employee_id]" value="'+ setOfIndexedEmployee[i] +'" />');
                $('form').append('<input type="hidden" name="evaluations['+ i +']['+ j +'][solution_id]" value="'+ setOfIndexedSolution[j] +'" />');
                
                if (!flag) {
                    tp.item(j).name = 'thresholds['+ j +'][p]';
                    tq.item(j).name = 'thresholds['+ j +'][q]';
                    ts.item(j).name = 'thresholds['+ j +'][s]';
                    tt.item(j).name = 'thresholds['+ j +'][type]';
                    te.item(j).name = 'thresholds['+ j +'][extrema]';
                    $('form').append('<input type="hidden" name="thresholds['+ j +'][solution_id]" value="'+ setOfIndexedSolution[j] +'" />');
                }
                j++;
            }
        }).catch(function (err) {});
    </script>
@endpush
