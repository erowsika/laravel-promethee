@extends('layouts.app')

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Evaluation Datetime</div>

                <div class="panel-body">
                    @if (session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif

                    <input type="datetime" name="datetime" id="datetime" value="{{ $evaluation['datetime'] }}">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Evaluation</div>
                    <table class="table" style="table-layout: fixed;">
                        <tr>
                            <td>Employee</td>
                            <td>RC<sub>i</sub><sub>+</sub></td>
                        </tr>
                        @foreach ($evaluation['results'] as $key => $value)
                        <tr>
                            <td>{{ $employees[$key]->name }}</td>
                            <td>{{ $value }}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script type="text/javascript">
    </script>
@endpush
