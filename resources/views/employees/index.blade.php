@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Employees - <span>{{ $employees->count() }}</span> <span><a href="{{ route('employee.create') }}" class="button">Create</a></span></div>

                <div class="panel-body">
                    @if (session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>NIP</th>
                                <th>Rank</th>
                                <th>Group</th>
                                <th>Position</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($employees as $employee)
                            <tr>
                                <td>{{ $employee->name }}</td>
                                <td>{{ $employee->nip  }}</td>
                                <td>{{ $employee->ranks->first()->name }}</td>
                                <td>{{ $employee->groups->first()->name }}</td>
                                <td>{{ $employee->position }}</td>
                                <td>
                                    <a href="{{ route('employee.edit', [ 'employee' => $employee->id ]) }}">Edit</a>
                                    <form action="{{ route('employee.destroy', [ 'employee' => $employee->id ]) }}" method="POST">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <input type="submit" value="Delete">
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $employees->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
