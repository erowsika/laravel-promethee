@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Evaluation Datetimes <span>{{ $evaluation_datetimes->count() }}</span></div>

                <div class="panel-body">
                    @if (session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Datetime</th>
                                <th>TOPSIS</th>
                                <th>Promethee</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($evaluation_datetimes as $value)
                            <tr>
                                <td>{{ $value->datetime }}</td>
                                <td><a href="{{ route('evaluation_datetime.show', [ 'evaluation_datetime' => $value->id, 'method' => 'topsis']) }}">T</a></td>
                                <td><a href="{{ route('evaluation_datetime.show', [ 'evaluation_datetime' => $value->id, 'method' => 'promethee']) }}">P</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $evaluation_datetimes->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
