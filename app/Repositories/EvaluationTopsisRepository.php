<?php

namespace App\Repositories;

use App\Core\Topsis;
use App\Models\Solution;
use App\Models\Employee;
use App\Models\EvaluationDatetime;

class EvaluationTopsisRepository
{
    protected $model;

    public function __construct(EvaluationDatetime $model) {
        $this->model = $model;
    }

    public function get($id)
    {
        $query = $this->model::findOrFail($id);

        $Mkriteria = Solution::all();
        $karyawan = Employee::all();

        $query2 = $query->evaluations()        
        ->join('employees', 'employees.id', '=', 'evaluations.employee_id')
        ->join('solutions', 'solutions.id', '=', 'evaluations.solution_id')
        ->selectRaw(
            'employees.name as nama_alternatif, solutions.name as nama_kriteria, evaluations.point as nilai, solutions.point as bobot'
        )
        ->get();
        
        $topsis = new Topsis($query2);

        return [
            'datetime' => $query->datetime,
            'results' => $topsis->get()
        ];
    }
}
