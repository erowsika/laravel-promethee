<?php

namespace App\Core;

class Topsis
{
    private $bobot;
    private $data;
    private $kriterias;
    private $nilai_kuadrat;

    private $temp;
    private $rijr;
    private $rij;
    private $y;
    private $yijr;
    private $aplus;
    private $aplusr;
    private $aminus;
    private $aminusr;
    private $displus;
    private $diplusr;
    private $rciplus;
    private $rciplusr;
    private $query;

    /**
     * Initilize
     *
     * @param \App\Models\EvaluationDatetime $query Default NULL
     * @return type
     **/
    public function __construct($query = null)
    {
        $this->query = $query;
        $this->data();
        $this->rij();
        $this->yij();
        $this->aplus();
        $this->aminus();
        $this->diplus();
        $this->diminus();
        $this->rciplus();
    }

    private function aplus()
    {
        $this->aplus = array();
        foreach ($this->kriteria as $k) {
            $this->temp = ( [$k] ? max($this->y[$k]) : min($this->y[$k]) );
            $this->aplusr[$k] = round($this->temp, 4);
            $this->aplus[$k] = $this->temp;
        }
    }

    private function aminus()
    {
        $this->aminus = array();
        foreach ($this->kriteria as $k) {
            $this->temp = ( [$k] ? min($this->y[$k]) : max($this->y[$k]) );
            $this->aminusr[$k] = round($this->temp, 4);
            $this->aminus[$k] = $this->temp;
        }
    }

    private function data()
    {
        foreach ($this->query as $index => $row) {
            if (!isset($this->data[$row->nama_alternatif])) $this->data[$row->nama_alternatif] = array();
            if (!isset($this->data[$row->nama_alternatif][$row->nama_kriteria])) $this->data[$row->nama_alternatif][$row->nama_kriteria] = array();
            if (!isset($this->nilai_kuadrat[$row->nama_kriteria])) $this->nilai_kuadrat[$row->nama_kriteria] = 0;

            $this->bobot[$row->nama_kriteria] = $row->bobot;
            $this->data[$row->nama_alternatif][$row->nama_kriteria] = $row->nilai;
            $this->nilai_kuadrat[$row->nama_kriteria] += pow($row->nilai,2);
            $this->kriterias[] = $row->nama_kriteria;
            $this->kriteria = array_unique($this->kriterias);
        }
    }

    private function diplus()
    {
        $this->diplus = array();
        $i=0;
        foreach ($this->data as $nama => $krit) {
            ++$i;
            foreach ($this->kriteria as $k) {
                if (!isset($this->diplusr[$i-1]) && !isset($this->diplus[$i-1])) $this->diplus[$i-1] = $this->this->diplusr[$i-1] = 0;
                $this->diplus[$i-1] += pow($this->aplus[$k] - $this->y[$k][$i-1], 2);
            }
            $this->temp = sqrt($this->diplus[$i-1]);
            $this->diplusr[$i-1] = round($this->temp, 4);
            $this->diplus[$i-1] = $this->temp;            
        }
    }

    private function diminus()
    {
        $this->diminus = array();
        $i=0;
        foreach ($this->data as $nama => $krit) {
            ++$i;
            foreach ($this->kriteria as $k) {
                if ( !isset($this->diminus[$i-1]) ) $this->diminus[$i-1] = 0;
                $this->diminus[$i-1] += pow($this->aminus[$k] - $this->y[$k][$i-1], 2);
            }
            $this->temp = sqrt($this->diminus[$i-1]);
            $this->diminusr[$i-1] = round($this->temp, 4);
            $this->diminus[$i-1] = $this->temp;
        }
    }

    private function rciplus()
    {
        $this->rciplus = array();
        $i=0;   
        foreach ($this->data as $nama => $krit) {
            ++$i;
            foreach ($this->kriteria as $k) {
                $this->temp =  $this->diminus[$i-1] / ( $this->diminus[$i-1] + $this->diplus[$i-1] );
                $this->rciplusr[$i-1] = round($this->temp, 4);
                $this->rciplus[$i-1] = $this->temp;
            }
        }
    }

    private function yij()
    {
        $i=0;
        $this->y = array();
        foreach ($this->data as $nama => $krit) {
            ++$i;
            foreach ($this->kriteria as $k) {
                $this->temp = $this->bobot[$k] * $this->rij[$nama][$k];
                $this->y[$k][$i-1] = $this->temp;
                $this->yijr[$nama][$k] = round($this->temp, 4);
                $this->yij[$nama][$k] = $this->temp;
            }
        }
    }

    private function rij()
    {
        foreach ($this->data as $nama => $krit) {
            foreach ($this->kriteria as $k) {
                $this->temp = $krit[$k] / sqrt($this->nilai_kuadrat[$k]);
                $this->rijr[$nama][$k] = round($this->temp, 4);
                $this->rij[$nama][$k] = $this->temp;
            }
        }
    }

    public function get()
    {
        return $this->rciplusr;
    }
}
