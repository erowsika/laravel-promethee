<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\UnautherizedOrdinaryRoleException;

class VerifyRoleAuthorized
{
    /**
     * @var string $message
     */
    protected $message = 'This authenticated user role is unautherized.';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     * @throws \App\Exceptions\UnautherizedOrdinaryRoleException
     */
    public function handle($request, Closure $next)
    {
        if (! Auth::user()->isSpecialRole()) {
            throw new UnautherizedOrdinaryRoleException($this->message);
        }

        return $next($request);
    }
}
