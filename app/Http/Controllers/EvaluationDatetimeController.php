<?php

namespace App\Http\Controllers;

use App\Models\Solution;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\EvaluationDatetime;
use App\Repositories\EvaluationTopsisRepository;

class EvaluationDatetimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $evaluation_datetimes = EvaluationDatetime::paginate(10);

        return View('evaluation_datetimes', [
            'evaluation_datetimes' => $evaluation_datetimes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     * 
     * @param \Illuminate\Http\Request $request
     * @param \App\Repositories\EvaluationTopsisRepository $evaluation_topsis
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id, EvaluationTopsisRepository $evaluation_topsis)
    {
        $request->validate([
            'method' => 'required|string|max:9'
        ]);
        
        $employees = Employee::all();
        $evaluation_topsis = $evaluation_topsis->get($id);

        switch ($request->method) {
            case 'promethee':
                return view('show_promethee');
            case 'topsis':
                return view('show_topsis', [
                    'employees' => $employees,
                    'evaluation' => $evaluation_topsis
                ]);
            default:
                return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
