<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Solution;
use App\Models\Threshold;
use App\Models\Evaluation;
use Illuminate\Http\Request;
use App\Models\EvaluationDatetime;
use App\Http\Requests\StoreEvaluationRequest;

class EvaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::all();
        $solutions = Solution::all();

        return view('create_evaluation', [
            'employees' => $employees,
            'solutions' => $solutions
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEvaluationRequest $request)
    {
        $employees = Employee::all()->count();
        $solutions = Solution::all()->count();
        $threshold_ids = [];
        $i=$j=$flag=0;

        $evaluation_datetime = EvaluationDatetime::create($request->all());

        for ($index=0; $index < $solutions * $employees; $index++) {
            if ($i >= $employees) $i = 0;
            if ($j >= $solutions) {
                $j = 0;
                $flag = 1;
                $i++;
            }
            if (!$flag) $threshold = Threshold::create($request->thresholds[$j]);
            array_push($threshold_ids, $threshold->id);

            $evaluation = Evaluation::create($request->evaluations[$i][$j]);
            $evaluation->evaluationDatetimes()->attach($evaluation_datetime->id);
            
            if ($j === $solutions - 1) {
                $evaluation->thresholds()->syncWithoutDetaching($threshold_ids);
            }
            $j++;
        }

        return redirect()->route('evaluation.create')->with('status', 'Successfully Created New Evaluation !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Retrive all primary key from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPrimaryKeys()
    {
        $employee_ids = Employee::all()->pluck('id');
        $solution_ids = Solution::all()->pluck('id');

        return response()->json([
            $employee_ids, $solution_ids            
        ]);
    }
}
