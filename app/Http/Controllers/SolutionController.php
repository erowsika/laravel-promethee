<?php

namespace App\Http\Controllers;

use App\Models\Solution;
use Illuminate\Http\Request;
use App\Http\Requests\Solutions\StoreSolutionRequest;
use App\Http\Requests\Solutions\UpdateSolutionRequest;

class SolutionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $solutions = Solution::paginate(10);

        return view('solutions.index', compact('solutions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('solutions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSolutionRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreSolutionRequest $request)
    {
        $solution = Solution::create($request->all());

        return redirect()->route('solution.create')->withSuccess('Sucessfully Created New Solution !');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Solution  $solution
     * @return \Illuminate\View\View
     */
    public function edit(Solution $solution)
    {
        return view('solutions.edit', compact('solution'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Solution  $solution
     * @param  \App\Http\Requests\UpdateSolutionRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Solution $solution, UpdateSolutionRequest $request)
    {
        $solution->update($request->all());

        return redirect()->route('solution.edit', $solution)->withSuccess('Successfully Updated Solution !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Solution  $solution
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Solution $solution)
    {
        $solution->delete();

        return redirect()->route('solution.index')->withSuccess('Successfully Deleted Solution !');
    }
}
