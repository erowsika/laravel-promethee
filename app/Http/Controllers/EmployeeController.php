<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Rank;
use App\Models\Group;
use Illuminate\Http\Request;
use App\Http\Requests\Employees\StoreEmployeeRequest;
use App\Http\Requests\Employees\UpdateEmployeeRequest;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $employees = Employee::with(['groups', 'ranks'])->paginate(10);

        return view('employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $groups = Group::all();
        $ranks = Rank::all();

        return view('employees.create', compact('groups', 'ranks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreEmployeeRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreEmployeeRequest $request)
    {
        $employee = Employee::create($request->all());

        $employee->groups()->sync($request->group);
        $employee->ranks()->sync($request->rank);

        return redirect()->route('employee.create')->withSuccess('Successfully Created New Employee !');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\View\View
     */
    public function edit(Employee $employee)
    {
        $employeeGroup = $employee->groups->first();
        $employeeRank = $employee->ranks->first();
        $groups = Group::all();
        $ranks = Rank::all();

        return view('employees.edit', compact(
            'employee',
            'employeeGroup',
            'employeeRank',
            'groups',
            'ranks'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Employee  $employee
     * @param  \App\Http\Requests\UpdateEmployeeRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Employee $employee, UpdateEmployeeRequest $request)
    {
        $employee->update($request->all());

        $employee->groups()->sync($request->group);

        $employee->ranks()->sync($request->rank);

        return redirect()->route('employee.edit', $employee)->withSuccess('Successfully Updated Employee !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();

        $employee->groups()->detach();

        $employee->ranks()->detach();

        return redirect()->route('employee.index')->withSuccess('Successfully Delete Employee !');
    }
}
