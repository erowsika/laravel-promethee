<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEvaluationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'datetime' => 'required|date',
            'evaluations' => 'required|array',
            'evaluations.*' => 'required|array',
            'evaluations.*.*' => 'required|array',
            'evaluations.*.*.point' => 'required|integer',
            'evaluations.*.*.employee_id' => 'required|integer',
            'evaluations.*.*.solution_id' => 'required|integer',
            'thresholds' => 'required|array',
            'thresholds.*' => 'required|array',
            'thresholds.*.extrema' => 'required|in:min,max',
            'thresholds.*.type' => 'required|integer',
            'thresholds.*.p' => 'required|integer',
            'thresholds.*.q' => 'required|integer',
            'thresholds.*.s' => 'required|integer',
            'thresholds.*.solution_id' => 'required|integer'
        ];
    }
}
