<?php

namespace App\Testing\Concerns;

use App\Http\Middleware\VerifyRoleAuthorized;

trait WithoutFeatureMiddleware
{
    /**
     * @var array $featureMiddlewares
     */
    protected $featureMiddlewares = [
        VerifyRoleAuthorized::class,
    ];

    /**
     * 
     * Disable middleware of this app that introduced as their feature
     * 
     * @return void
     */
    protected function disableFeatureMiddleware()
    {
        $this->withoutMiddleware($this->featureMiddlewares);
    }
}
