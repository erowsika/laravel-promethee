<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'nip', 'position'
    ];

    public function groups() {
        return $this->belongsToMany('App\Models\Group');
    }

    public function ranks() {
        return $this->belongsToMany('App\Models\Rank');
    }
}
