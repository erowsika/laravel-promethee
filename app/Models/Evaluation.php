<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id', 'solution_id', 'point'
    ];

    public function evaluationDatetimes()
    {
        return $this->belongsToMany('App\Models\EvaluationDatetime');
    }

    public function thresholds()
    {
        return $this->belongsToMany('App\Models\Threshold');
    }

    public function employee()
    {
        return $this->belongsTo('App\Models\Employee');
    }
}
