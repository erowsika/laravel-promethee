<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EvaluationDatetime extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'datetime'
    ];

    public function evaluations()
    {
        return $this->belongsToMany('App\Models\Evaluation');
    }
}
