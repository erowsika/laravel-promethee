Master branch used for fake-production as it run in real server.  
0.1 branch for development.  

## Installation :

clone this repo

```bash
git clone -b master https://gitlab.com/erowsika/laravel-promethee
```

set up env variable.

```bash
cp .env.example .env
```

run requiring depedency.

```bash
composer install --no-dev
```

next run database migration & seeding.

```bash
php artisan migrate --seed
```

after all those, inside `.env` fill that variable accordingly.  

----------

## Testing

this steps applied with assumption it fresh clone from repo.

## Pre testing

- Set up env file

```bash
cp .env.testing .env
```

- run require dependency

```bash
composer install
```

- Set up database

To keep things simple use sqlite instead.

```bash
touch database/database.sqlite
```

- Set up webserver

run php webserver.

```bash
php artisan serve
```

## Testing

1. Http testing

run phpunit use env file that used by php webserver too.

```bash
./vendor/bin/phpunit
```

2. Browser testing

run dusk use env file that used by php webserver.

```bash
php artisan dusk
```